Biblioteka powstała jako część pracy inżynierskiej i zawiera podstawowe funkcje służące do reprezentacji grafów oraz ich wizualizacji. Dodatkowo zaimplementowano kilka podstawowych algorytmów grafowych. 

Całość opisana w wiki, oraz w pliku .pdf zawierającym całą pracę inżynierską.