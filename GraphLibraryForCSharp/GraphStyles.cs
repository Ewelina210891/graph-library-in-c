﻿using System.Collections.Generic;
using System.Drawing;

namespace GraphLibraryForCSharp
{
    public class GraphStyles
    {
        /// <summary>
        /// Obiekt klasy SolidBrush odpowiedzialny za rysowanie punktów oznaczających wierzchołki.
        /// </summary>
        public SolidBrush VerticePointBrush { get; set; }
        /// <summary>
        /// Obiekt klasy SolidBrush odpowiedzialny za rysowanie nazw wierzchołków.
        /// </summary>
        public SolidBrush VerticeNameBrush { get; set; }
        /// <summary>
        /// Obiekt klasy Font odpowiedzialny za czcionkę użytą przy rysowaniu nazw wierzchołków.
        /// </summary>
        public Font VerticeNameFont { get; set; }
        /// <summary>
        /// Obiekt klasy SolidBrush odpowiedzialny za rysowanie wag wierzchołków (w algorytmie Dijkstry).
        /// </summary>
        public SolidBrush VerticeWeightBrush { get; set; }
        /// <summary>
        /// Obiekt klasy Font odpowiedzialny za czcionkę użytą przy rysowaniu wag wierzchołków (w algorytmie Dijkstry).
        /// </summary>
        public Font VerticeWeightFont { get; set; }
        /// <summary>
        /// Obiekt klasy Solid Brush odpowiedzialny za rysowanie wag krawędzi.
        /// </summary>
        public SolidBrush WeightBrush { get; set; }
        /// <summary>
        /// Obiekt klasy Font odpowiedzialny za czcionkę użytą przy rysowaniu wag krawędzi.
        /// </summary>
        public Font WeightFont { get; set; }
        /// <summary>
        /// Obiekt klasy Pen odpowiedzialny za rysowanie krawędzi.
        /// </summary>
        public Pen EdgePen { get; set; }
        /// <summary>
        /// Lista obiektów klasy Pen odpowiedzialnych za rysowanie krawędzi (w algorytmie BFS oraz DFS).
        /// </summary>
        public List<Pen> EdgePenList { get; set; }
    }
}