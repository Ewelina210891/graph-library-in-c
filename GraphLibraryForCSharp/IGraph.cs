﻿using System.Collections.Generic;
using System.Drawing;

namespace GraphLibraryForCSharp
{
    public interface IGraph
    {
        /// <summary>
        /// Określa, czy graf jest grafem ważonym.
        /// </summary>
        bool IsWeighted { get; }

        /// <summary>
        /// Rysowanie grafu na okręgu.
        /// </summary>
        /// <param name="pictureOfGraph">Obiekt klasy Graphics, na którym będzie rysowany graf.</param>   
        void DrawGraphOnCircle(Graphics pictureOfGraph);

        /// <summary>
        /// Dodanie wagi dla wybranej krawędzi (domyślnie waga wynosi 1).
        /// </summary>
        /// <param name="edge">Wybrana krawędź.</param>
        /// <param name="weight">Wartość liczbowa określająca wagę.</param>
        void AddWeight(Edge edge, double weight);

        /// <summary>
        /// Dodanie wag dla wszystkich krawędzi w grafie za pomocą macierzy wag (domyślnie waga każdej krawędzi wynosi 1). 
        /// </summary>
        /// <param name="weights">Macierz wag</param>
        void AddAllWeights(double[,] weights);

        /// <summary>
        /// Usunięcie wszystkich wag (ustawienie każdej krawędzi domyślnej wagi 1).
        /// </summary>
        void RemoveAllWeights();

        /// <summary>
        /// Dodanie nowego wierzchołka do grafu.
        /// </summary>
        /// <param name="vertex">Wierzchołek, który ma zostać dodany.</param>
        void AddVertex(Vertex vertex);

        /// <summary>
        /// Usunięcie wybranego wierzchołka z grafu (krawędzie połączone z tym wierzchołkiem zostaną usunięte).
        /// </summary>
        /// <param name="vertex">Wierzchołek, który ma zostać usunięty.</param>
        void RemoveVertex(Vertex vertex);

        /// <summary>
        /// Usunięcie wszystkich wierzchołków z grafu (automatycznie usuwa wszystkie krawędzie).
        /// </summary>
        void RemoveAllVertex();

        /// <summary>
        /// Dodanie nowej krawędzi do istniejącego grafu.
        /// </summary>
        /// <param name="edge">Krawędź, która ma zostać dodana.</param>
        void AddEdge(Edge edge);

        /// <summary>
        /// Usunięcie wybranej krawędzi z grafu.
        /// </summary>
        /// <param name="edge">Krawędź, która ma zostać usunięta.</param>
        void RemoveEdge(Edge edge);

        /// <summary>
        /// Usunięcie wszystkich krawędzi z grafu (stworzenie grafu pustego).
        /// </summary>
        void RemoveAllEdges();

        /// <summary>
        /// Pobranie dowolnej krawędzi z grafu.
        /// </summary>
        /// <param name="vertexBegin">Wierzchołek początkowy krawędzi.</param>
        /// <param name="vertexEnd">Wierzchołek końcowy krawędzi.</param>
        /// <returns>Zwraca krawędź o podanym początku i końcu.</returns>
        Edge GetEdge(Vertex vertexBegin, Vertex vertexEnd);

        /// <summary>
        /// Pobranie dowolnego wierzchołka z grafu.
        /// </summary>
        /// <param name="vertexName">Nazwa wierzchołka.</param>
        /// <returns>Zwraca wierzchołek o podanej nazwie.</returns>
        Vertex GetVertex(string vertexName);

        /// <summary>
        /// Pobranie listy wszystkich wierzchołków grafu.
        /// </summary>
        /// <returns>Zwraca listę zawierającą wszystkie wierzchołki grafu.</returns>
        List<Vertex> GetAllVerticesToList();

        /// <summary>
        /// Pobranie listy wszystkich krawędzi grafu.
        /// </summary>
        /// <returns>Zwraca listę zawierającą wszystkie krawędzie grafu.</returns>
        List<Edge> GetAllEdgesToList();

        /// <summary>
        /// Pobranie macierzy sąsiedztwa.
        /// </summary>
        /// <returns>Zwraca macierz sąsiedztwa.</returns>
        int[,] GetAdjacencyMatrix();

        /// <summary>
        /// Pobranie macierzy wag.
        /// </summary>
        /// <returns>Zwraca macierz wag.</returns>
        double[,] GetMatrixOfWeights();

        /// <summary>
        /// Uruchamia algorytm przeszukiwania wszerz (BFS).
        /// </summary>
        /// <param name="vertex">Wierzchołek startowy algorytmu (źródło).</param>
        /// <returns>Zwraca listę wierzchołków należących do drzewa przeszukiwania wszerz.</returns>
        List<Vertex> BfsAlgorithm(Vertex vertex);

        /// <summary>
        /// Uruchamia algorytm przeszukiwania w głąb (DFS).
        /// </summary>
        /// <param name="vertex">Wierzchołek startowy algorytmu (źródło).</param>
        /// <returns>Zwraca listę wierzchołków należących do lasu przeszukiwania w głąb.</returns>
        List<Vertex> DfsAlgorithm(Vertex vertex);

        /// <summary>
        /// Uruchamia algorytm Prima.
        /// </summary>
        /// <param name="vertex">Wierzchołek startowy algorytmu (źródło).</param>
        /// <returns>Zwraca listę wierzchołków należących do drzewa wynikowego.</returns>
        List<Vertex> PrimsAlgorithm(Vertex vertex);

        /// <summary>
        /// Uruchamia algorytm Dijkstry.
        /// </summary>
        /// <param name="vertex">Wierzchołek startowy grafu (źródło).</param>
        /// <returns>Zwraca listę wierzchołków należących do drzewa wynikowego.</returns>
        List<Vertex> DijkstrasAlgorithm(Vertex vertex);

        /// <summary>
        /// Pokazuje w formie graficznej wynik algorytmu przeszukiwania wszerz (BFS).
        /// </summary>
        /// <param name="vertices">Lista wierzchołków zwrócona jako wynik działania algorytmu BFS.</param>
        /// <param name="pictureOfGraph">Obiekt klasy Graphics, na którym będzie rysowany graf wynikowy.</param>
        /// <param name="time">Czas w milisekundach określający, w jakim odstępie czasowym mają być rysowane kolejne krawędzie.</param>
        void ShowBfsResult(List<Vertex> vertices, Graphics pictureOfGraph, int time);

        /// <summary>
        /// Pokazuje w formie graficznej wynik algorytmu przeszukiwania w głąb (DFS).
        /// </summary>
        /// <param name="vertices">Lista wierzchołków zwrócona jako wynik działania algorytmu DFS.</param>
        /// <param name="pictureOfGraph">Obiekt klasy Graphics, na którym będzie rysowany graf.</param>
        /// <param name="time">Czas w milisekundach określający w jakim odstępie czasowym mają być rysowane kolejne krawędzie.</param>
        void ShowDfsResult(List<Vertex> vertices, Graphics pictureOfGraph, int time);

        /// <summary>
        /// Pokazuje w formie graficznej wynik algorytmu Prima.
        /// </summary>
        /// <param name="vertices">Lista wierzchołków zwrócona jako wynik działania algorytmu Prima.</param>
        /// <param name="pictureOfGraph">Obiekt klasy Graphics, na którym będzie rysowany graf.</param>
        /// <param name="time">Czas w milisekundach określający w jakim odstępie czasowym mają być rysowane kolejne krawędzie.</param>
        void ShowPrimsResult(List<Vertex> vertices, Graphics pictureOfGraph, int time);

        /// <summary>
        /// Pokazuje w formie graficznej wynik działania algorytmu Dijkstry.
        /// </summary>
        /// <param name="vertices">Lista wierzchołków zwrócona jako wynik działania algorytmu Dijkstry.</param>
        /// <param name="pictureOfGraph">Obiekt klasy Graphics, na którym będzie rysowany graf.</param>
        /// <param name="time">Czas w milisekundach określający w jakim odstępie czasowym mają być rysowane kolejne krawędzie.</param>
        void ShowDijkstrasResult(List<Vertex> vertices, Graphics pictureOfGraph, int time);

        /// <summary>
        /// Ustawia domyślne wartości styli.
        /// </summary>
        void SetDefaultStyles();

        /// <summary>
        /// Ustawia wartości styli podane przez użytkownika.
        /// </summary>
        /// <param name="graphStyles">Obiekt klasy GraphStyles z ustawionymi przez użytkownika wartościami styli.</param>
        void SetCustomStyles(GraphStyles graphStyles);
    }
}