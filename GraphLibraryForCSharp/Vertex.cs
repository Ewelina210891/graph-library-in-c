﻿using System.Collections.Generic;

namespace GraphLibraryForCSharp
{
    public class Vertex
    {
        /// <summary>
        /// Tworzy nowy wierzchołek.
        /// </summary>
        /// <param name="name">Nazwa wierzchołka.</param>
        public Vertex(string name)
        {
            Name = name;
            Neighbors = new List<Vertex>();
        }

        /// <summary>
        /// Nazwa wierzchołka.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Współrzędna X wierzchołka (wyliczana przy rysowaniu).
        /// </summary>
        public int X { get; internal set; }

        /// <summary>
        /// Współrzędna Y wierzchołka (wyliczana przy rysowaniu).
        /// </summary>
        public int Y { get; internal set; }

        /// <summary>
        /// Lista sąsiedztwa.
        /// </summary>
        internal List<Vertex> Neighbors { get; set; }

        /// <summary>
        /// Odległość (pole wykorzystywane w algorytmach).
        /// </summary>
        public double Distance { get; internal set; }

        /// <summary>
        /// Poprzednik (pole wykorzystywane w algorytmach).
        /// </summary>
        public Vertex Parent { get; internal set; }
        /// <summary>
        /// Kolor wierzchołka (pole wykorzystywane w algorytmach).
        /// </summary>
        internal string Colour { get; set; }

        /// <summary>
        /// Czas przetworzenia (pole wykorzystywane w algorytmach).
        /// </summary>
        public int ProcessTime { get; internal set; }

        /// <summary>
        /// Czas odwiedzenia (pole wykorzystywane w algorytmach).
        /// </summary>
        public int VisitTime { get; internal set; }

        /// <summary>
        /// Klucz (pole wykorzystywane w algorytmach).
        /// </summary>
        public double Key { get; internal set; }

        /// <summary>
        /// Pobranie listy sąsiedztwa dla danego wierzchołka.
        /// </summary>
        /// <returns>Zwraca listę sąsiedztwa.</returns>
        public List<Vertex> GetListOfNeighbors()
        {
            return Neighbors;
        }
    }
}