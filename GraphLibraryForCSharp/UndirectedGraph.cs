﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;

namespace GraphLibraryForCSharp
{
    public class UndirectedGraph : IGraph
    {
        #region Variables

        private readonly GraphStyles _graphStyles = new GraphStyles();
        private int[,] _adjacencyMatrix;

        private double _angleOfDraw;
        private Point _centerPointOfDraw;
        private List<Edge> _edges = new List<Edge>();
        private double[,] _matrixOfWeights;
        private int _radiusXOfDraw;
        private int _radiusYOfDraw;

        private int _stepDfsAlgorithm;
        private List<Vertex> _vertices = new List<Vertex>();
        public bool IsWeighted { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Tworzy nowy graf bez wag za pomocą macierzy sąsiedztwa. Nazwy wierzchołków ustawiane są domyślnie.
        /// </summary>
        /// <param name="adjacencyMatrix">Macierz sąsiedztwa (musi być kwadratowa).</param>
        public UndirectedGraph(int[,] adjacencyMatrix)
        {
            if (adjacencyMatrix.GetLength(0) != adjacencyMatrix.GetLength(1)) throw new ArgumentException("Błąd: macierz sąsiedztwa musi być macierzą kwadratową!");
            var countOfVertices = adjacencyMatrix.GetLength(0);

            FillAdjacencyMatrix(countOfVertices, adjacencyMatrix);
            FillMatrixOfWeights();
            FillListOfVertices(countOfVertices);
            FillListOfEdges(countOfVertices);
            SetDefaultStyles();

            IsWeighted = false;
        }

        /// <summary>
        /// Tworzy nowy graf bez wag za pomocą macierzy sąsiedztwa. Nazwy wierzchołków są podawane jako argument (musi być ich tyle, co wierzchołków).
        /// </summary>
        /// <param name="adjacencyMatrix">Macierz sąsiedztwa (musi być kwadratowa).</param>
        /// <param name="namesOfVertices">Lista nazw wierzchołków (musi być ich dokładnie tyle, co wymiar macierzy sąsiedztwa).</param>
        public UndirectedGraph(int[,] adjacencyMatrix, List<string> namesOfVertices)
        {
            namesOfVertices = namesOfVertices.Distinct().ToList();
            if (adjacencyMatrix.GetLength(0) != adjacencyMatrix.GetLength(1))throw new ArgumentException("Błąd: macierz sąsiedztwa musi być macierzą kwadratową!");
            if (namesOfVertices.Count != adjacencyMatrix.GetLength(0))throw new ArgumentException("Błąd: lista nazw wierzchołków musi zawierać tyle elementów, ile jest wierzchołków! Sprawdź, czy lista zawiera odpowiednią liczbę elementów i czy nazwy nie powtarzają się (powtarzające się nazwy są automatycznie usuwane).");
            int countOfVertices = adjacencyMatrix.GetLength(0);

            FillAdjacencyMatrix(countOfVertices, adjacencyMatrix);
            FillMatrixOfWeights();
            FillListOfVertices(countOfVertices, namesOfVertices);
            FillListOfEdges(countOfVertices);
            SetDefaultStyles();
            IsWeighted = false;
        }

        /// <summary>
        /// Tworzy nowy graf ważony za pomocą macierzy wag. Nazwy wierzchołków ustawiane są domyślnie.
        /// </summary>
        /// <param name="matrixOfWeights">Macierz wag.</param>
        public UndirectedGraph(double[,] matrixOfWeights)
        {
            if (matrixOfWeights.GetLength(0) != matrixOfWeights.GetLength(1))
                throw new ArgumentException("Błąd: macierz wag musi być macierzą kwadratową!");
            int countOfVertices = matrixOfWeights.GetLength(0);

            FillAdjacencyMatrixFromMatrixOfWeights(countOfVertices, matrixOfWeights);
            FillMatrixOfWeights(countOfVertices, matrixOfWeights);
            FillListOfVertices(countOfVertices);
            FillListOfEdges(countOfVertices);
            SetDefaultStyles();

            IsWeighted = true;
        }

        /// <summary>
        /// Tworzy nowy graf ważony za pomocą macierzy wag. Nazwy wierzchołków są podawane jako argument (musi być ich tyle co wierzchołków).
        /// </summary>
        /// <param name="matrixOfWeights">Macierz wag.</param>
        /// <param name="namesOfVertices">Lista nazw wierzchołków (musi być ich dokładnie tyle co wymiar macierzy wag).</param>
        public UndirectedGraph(double[,] matrixOfWeights, List<string> namesOfVertices)
        {
            namesOfVertices = namesOfVertices.Distinct().ToList();
            if (matrixOfWeights.GetLength(0) != matrixOfWeights.GetLength(1)) throw new ArgumentException("Błąd: macierz wag musi być macierzą kwadratową!");
            if (namesOfVertices.Count != matrixOfWeights.GetLength(0)) throw new ArgumentException("Błąd: lista nazw wierzchołków musi zawierać tyle elementów, ile jest wierzchołków! Sprawdź, czy lista zawiera odpowiednią liczbę elementów i czy nazwy nie powtarzają się (powtarzające się nazwy są automatycznie usuwane).");
            var countOfVertices = matrixOfWeights.GetLength(0);

            FillAdjacencyMatrixFromMatrixOfWeights(countOfVertices, matrixOfWeights);
            FillMatrixOfWeights(countOfVertices, matrixOfWeights);
            FillListOfVertices(countOfVertices, namesOfVertices);
            FillListOfEdges(countOfVertices);
            SetDefaultStyles();

            IsWeighted = true;
        }

        /// <summary>
        /// Tworzy nowy pusty obiekt klasy UndirectedGraph.
        /// </summary>
        public UndirectedGraph()
        {
            SetDefaultStyles();
            IsWeighted = false;
        }

        #endregion

        #region DrawingPrivateMethods

        private void DrawEdges(Graphics pictureOfGraph)
        {
            foreach (var e in _edges)
            {
                pictureOfGraph.DrawLine(_graphStyles.EdgePen, e.Begin.X, e.Begin.Y, e.End.X, e.End.Y);
            }
        }

        private void DrawEdge(Graphics pictureOfGraph, Vertex vertexChild, Vertex vertexParent, Pen edgePen)
        {
            pictureOfGraph.DrawLine(edgePen, vertexParent.X, vertexParent.Y, vertexChild.X, vertexChild.Y);
        }

        private void DrawWeights(Graphics pictureOfGraph)
        {
            if (IsWeighted)
            {
                foreach (var edge in _edges)
                {
                    var x = (((edge.Begin.X + edge.End.X)/2) + edge.End.X)/2;
                    var y = ((((edge.Begin.Y + edge.End.Y)/2) + edge.End.Y)/2) - 5;
                    pictureOfGraph.DrawString(edge.Weight.ToString(), _graphStyles.WeightFont, _graphStyles.WeightBrush,
                                              x, y);
                }
            }
        }

        private void DrawWeight(Graphics pictureOfGraph, Vertex begin, Vertex end)
        {
            if (IsWeighted)
            {
                var edge =_edges.FirstOrDefault(z => (z.Begin.Name == begin.Name && z.End.Name == end.Name) || (z.Begin.Name == end.Name && z.End.Name == begin.Name));
                
                var x = (((begin.X + end.X)/2) + end.X)/2;
                var y = ((((begin.Y + end.Y)/2) + end.Y)/2) - 5;

                pictureOfGraph.DrawString(edge.Weight.ToString(), _graphStyles.WeightFont, _graphStyles.WeightBrush, x,y);
            }
        }

        private void DrawVertices(Graphics pictureOfGraph)
        {
            for (var verticeNumber = 0; verticeNumber < _vertices.Count; verticeNumber++)
            {
                DrawVertexPoint(pictureOfGraph, verticeNumber);
                DrawVertexName(pictureOfGraph, verticeNumber);
            }
        }

        private void DrawVertex(Graphics pictureOfGraph, Vertex vertex)
        {
            pictureOfGraph.FillEllipse(_graphStyles.VerticePointBrush, vertex.X - 7, vertex.Y - 7, 12, 12);
        }

        private void DrawVertexName(Graphics pictureOfGraph, int i)
        {
            var x = (int) (Math.Cos(i*_angleOfDraw)*(_radiusXOfDraw + 7) + _centerPointOfDraw.X);
            var y = (int) (Math.Sin(i*_angleOfDraw)*(_radiusYOfDraw + 7) + _centerPointOfDraw.Y);

            pictureOfGraph.DrawString(_vertices[i].Name, _graphStyles.VerticeNameFont, _graphStyles.VerticeNameBrush, x,y);
        }

        private void DrawVertexPoint(Graphics pictureOfGraph, int verticeNumber)
        {
            var x = _vertices[verticeNumber].X - 7;
            var y = _vertices[verticeNumber].Y - 7;
            pictureOfGraph.FillEllipse(_graphStyles.VerticePointBrush, x, y, 12, 12);
        }

        private void DrawDijkstrasWeightOfVertex(Graphics pictureOfGraph, Vertex vertex)
        {
            pictureOfGraph.DrawString(vertex.Distance.ToString(), _graphStyles.VerticeWeightFont,_graphStyles.VerticeWeightBrush, vertex.X - 30, vertex.Y - 35);
        }

        #endregion

        #region OtherPrivateMethods

        private List<Vertex> CopyOfTheListOfVertices()
        {
            var newListOfVertices = _vertices.Select(vertex => new Vertex(vertex.Name)
                                                                   {
                                                                       X = vertex.X, 
                                                                       Y = vertex.Y, 
                                                                       Neighbors = new List<Vertex>()
                                                                   }).ToList();

            foreach (var vertex in _vertices)
            {
                var newVertex = newListOfVertices.FirstOrDefault(x => x.Name == vertex.Name);

                foreach (var neighbor in vertex.Neighbors)
                {
                    var newNeighbor = newListOfVertices.FirstOrDefault(x => x.Name == neighbor.Name);
                    newVertex.Neighbors.Add(newNeighbor);
                }
            }
            return newListOfVertices;
        }

        private void FillAdjacencyMatrix(int countOfVertices, int[,] matrix)
        {
            _adjacencyMatrix = new int[countOfVertices,countOfVertices];

            for (var i = 0; i < matrix.GetLength(0); i++)
                for (var j = i + 1; j < matrix.GetLength(1); j++)
                    if (matrix[i, j] != 1 || i == j) _adjacencyMatrix[i, j] = 0;
                    else _adjacencyMatrix[i, j] = 1;
        }

        private void FillAdjacencyMatrixFromMatrixOfWeights(int countOfVertices, double[,] matrixOfWeights)
        {
            _adjacencyMatrix = new int[countOfVertices,countOfVertices];

            for (var i = 0; i < matrixOfWeights.GetLength(0); i++)
                for (var j = i + 1; j < matrixOfWeights.GetLength(1); j++)
                {
                    if (matrixOfWeights[i, j] == 0 || i == j) _adjacencyMatrix[i, j] = 0;
                    else _adjacencyMatrix[i, j] = 1;
                }
        }

        private void AddVertexToAdjacencyMatrix(int countOfVertices)
        {
            var newAdjacencyMatrix = new int[countOfVertices + 1,countOfVertices + 1];

            for (var i = 0; i < countOfVertices; i++)
                for (var j = i + 1; j < countOfVertices; j++)
                    newAdjacencyMatrix[i, j] = _adjacencyMatrix[i, j];

            _adjacencyMatrix = newAdjacencyMatrix;
        }

        private void RemoveVertexFromAdjacencyMatrix(int countOfVertices, int indexOfVertex)
        {
            var newAdjacencyMatrix = new int[countOfVertices - 1,countOfVertices - 1];

            var newIIndex = 0;
            for (var iIndex = 0; iIndex < countOfVertices; iIndex++)
            {
                if (iIndex != indexOfVertex)
                {
                    var newJIndex = newIIndex + 1;
                    for (var jIndex = iIndex + 1; jIndex < countOfVertices; jIndex++)
                    {
                        if (jIndex != indexOfVertex)
                        {
                            newAdjacencyMatrix[newIIndex, newJIndex] = _adjacencyMatrix[iIndex, jIndex];
                            newJIndex++;
                        }
                    }
                    newIIndex++;
                }
            }
            _adjacencyMatrix = newAdjacencyMatrix;
        }

        private void FillMatrixOfWeights()
        {
            var countOfVertices = _adjacencyMatrix.GetLength(0);
            _matrixOfWeights = new double[countOfVertices,countOfVertices];

            for (var i = 0; i < _matrixOfWeights.GetLength(0); i++)
                for (var j = i + 1; j < _matrixOfWeights.GetLength(1); j++)
                    if (i == j) _matrixOfWeights[i, j] = 0;
                    else _matrixOfWeights[i, j] = 1;
        }

        private void FillMatrixOfWeights(int countOfVertices, double[,] matrix)
        {
            _matrixOfWeights = new double[countOfVertices,countOfVertices];

            for (var i = 0; i < _matrixOfWeights.GetLength(0); i++)
                for (var j = i + 1; j < _matrixOfWeights.GetLength(1); j++)
                    if (i == j) _matrixOfWeights[i, j] = 0;
                    else _matrixOfWeights[i, j] = matrix[i, j];
        }

        private void AddVertexToMatrixOfWeights(int countOfVertices)
        {
            var newMatrixOfWeights = new double[countOfVertices+1,countOfVertices+1];

            for (var i = 0; i < _vertices.Count; i++)
                for (var j = i + 1; j < _vertices.Count; j++)
                    newMatrixOfWeights[i, j] = _matrixOfWeights[i, j];

            _matrixOfWeights = newMatrixOfWeights;
        }

        private void RemoveVertexFromMatrixOfWeights(int countOfVertices, int indexOfVertex)
        {
            var newMatrixOfWeights = new double[countOfVertices - 1,countOfVertices - 1];

            var newIIndex = 0;
            for (var iIndex = 0; iIndex < countOfVertices; iIndex++)
            {
                if (iIndex != indexOfVertex)
                {
                    var newJIndex = newIIndex + 1;
                    for (var jIndex = iIndex + 1; jIndex < countOfVertices; jIndex++)
                    {
                        if (jIndex != indexOfVertex)
                        {
                            newMatrixOfWeights[newIIndex, newJIndex] = _matrixOfWeights[iIndex, jIndex];
                            newJIndex++;
                        }
                    }
                    newIIndex++;
                }
            }
            _matrixOfWeights = newMatrixOfWeights;
        }

        private void FillListOfVertices(int countOfVertices, List<string> namesOfVertices)
        {
            for (var numberOfVertex = 0; numberOfVertex < countOfVertices; numberOfVertex++)
            {
                var vertex = new Vertex(namesOfVertices[numberOfVertex]);
                _vertices.Add(vertex);
            }
        }

        private void FillListOfVertices(int countOfVertices)
        {
            for (var numberOfVertex = 0; numberOfVertex < countOfVertices; numberOfVertex++)
            {
                var vertex = new Vertex("v" + numberOfVertex);
                _vertices.Add(vertex);
            }
        }

        private void FillListOfEdges(int countOfVertices)
        {
            for (var i = 0; i < countOfVertices; i++)
            {
                for (var j = i + 1; j < countOfVertices; j++)
                {
                    if (_adjacencyMatrix[i, j] != 0 && i != j)
                    {
                        var edge = new Edge(_vertices[i], _vertices[j])
                                    {
                                        Weight = _matrixOfWeights[i, j]
                                    };
                        _edges.Add(edge);

                        _vertices[i].Neighbors.Add(_vertices[j]);
                        _vertices[j].Neighbors.Add(_vertices[i]);
                    }
                }
            }
        }

        private void CalculateCoordinates(int verticesCount, int panelWidth, int panelHeight)
        {
            if (verticesCount == 0) _angleOfDraw = 0;
            else _angleOfDraw = (2*Math.PI)/verticesCount;

            _centerPointOfDraw = new Point(panelWidth/2, panelHeight/2);
            _radiusXOfDraw = 4*panelWidth/10;
            _radiusYOfDraw = 4*panelHeight/10;

            for (var numberOfVertex = 0; numberOfVertex < verticesCount; numberOfVertex++)
            {
                _vertices[numberOfVertex].X = (int) (Math.Cos(numberOfVertex*_angleOfDraw)*_radiusXOfDraw + _centerPointOfDraw.X);
                _vertices[numberOfVertex].Y = (int) (Math.Sin(numberOfVertex*_angleOfDraw)*_radiusYOfDraw + _centerPointOfDraw.Y);
            }
        }

        private void RemoveNeighbor(Vertex vertex)
        {
            foreach (var neighbor in vertex.GetListOfNeighbors())
            {
                neighbor.Neighbors.Remove(vertex);
            }
        }

        private void RemoveEdgesFromVertex(Vertex vertex)
        {
            var edgesFromVertex = _edges.FindAll(x => (x.Begin == vertex || x.End == vertex));

            foreach (var edge in edgesFromVertex)
            {
                _edges.Remove(edge);
            }
        }

        #endregion

        #region PublicMethods

        public void DrawGraphOnCircle(Graphics pictureOfGraph)
        {
            if (pictureOfGraph == null) throw new ArgumentException("Błąd: element klasy Graphics nie może być nullem!");
            CalculateCoordinates(_vertices.Count, (int) pictureOfGraph.VisibleClipBounds.Width, (int) pictureOfGraph.VisibleClipBounds.Height);
            DrawEdges(pictureOfGraph);
            DrawVertices(pictureOfGraph);
            DrawWeights(pictureOfGraph);
        }

        public void SetDefaultStyles()
        {
            _graphStyles.VerticePointBrush = new SolidBrush(Color.FromArgb(255, 24, 101, 28));
            _graphStyles.VerticeNameBrush = new SolidBrush(Color.Black);
            _graphStyles.VerticeNameFont = new Font("Times New Roman", 22, FontStyle.Bold);
            _graphStyles.VerticeWeightBrush = new SolidBrush(Color.Red);
            _graphStyles.VerticeWeightFont = new Font("Times New Roman", 18, FontStyle.Regular);
            _graphStyles.EdgePen = new Pen(Color.FromArgb(255, 88, 228, 115), 1);
            _graphStyles.WeightBrush = new SolidBrush(Color.Blue);
            _graphStyles.WeightFont = new Font("Times New Roman", 16, FontStyle.Regular);

            _graphStyles.EdgePenList = new List<Pen>
                                           {
                                               _graphStyles.EdgePen
                                           };
        }

        public void SetCustomStyles(GraphStyles graphStyles)
        {
            if (graphStyles.VerticePointBrush != null) _graphStyles.VerticePointBrush = graphStyles.VerticePointBrush;
            if (graphStyles.VerticeNameBrush != null) _graphStyles.VerticeNameBrush = graphStyles.VerticeNameBrush;
            if (graphStyles.VerticeNameFont != null) _graphStyles.VerticeNameFont = graphStyles.VerticeNameFont;
            if (graphStyles.VerticeWeightBrush != null) _graphStyles.VerticeWeightBrush = graphStyles.VerticeWeightBrush;
            if (graphStyles.VerticeWeightFont != null) _graphStyles.VerticeWeightFont = graphStyles.VerticeWeightFont;
            if (graphStyles.EdgePen != null) _graphStyles.EdgePen = graphStyles.EdgePen;
            if (graphStyles.WeightBrush != null) _graphStyles.WeightBrush = graphStyles.WeightBrush;
            if (graphStyles.WeightFont != null) _graphStyles.WeightFont = graphStyles.WeightFont;
            if (graphStyles.EdgePenList != null)
            {
                if (graphStyles.EdgePenList.Count < 1)
                    throw new ArgumentException("Błąd: lista kolorów krawędzi nie może być pusta.");
                _graphStyles.EdgePenList = graphStyles.EdgePenList;
            }
        }

        public void AddWeight(Edge edge, double weight)
        {
            if (edge == null || edge.Begin == null || edge.End == null) throw new ArgumentException("Błąd: podana krawędź nie może być nullem.");

            var findEdge =_edges.FirstOrDefault(x =>(x.Begin.Name == edge.Begin.Name && x.End.Name == edge.End.Name) ||(x.Begin.Name == edge.End.Name && x.End.Name == edge.Begin.Name));
            if (findEdge == null) throw new ArgumentException("Błąd: podana krawędź nie istnieje w aktualnym grafie.");

            if (IsWeighted == false)
            {
                IsWeighted = true;
                FillMatrixOfWeights();
            }

            findEdge.Weight = weight;

            var iIndex = _vertices.IndexOf(findEdge.Begin);
            var jIndex = _vertices.IndexOf(findEdge.End);
            _matrixOfWeights[iIndex, jIndex] = weight;
        }

        public void AddAllWeights(double[,] weights)
        {
            if (weights == null) throw new ArgumentException("Błąd: macierz wag nie może być nullem.");
            if (weights.GetLength(0) != _adjacencyMatrix.GetLength(0) ||
                weights.GetLength(1) != _adjacencyMatrix.GetLength(1))
                throw new ArgumentException("Błąd: macierz wag musi mieć wymiar ilość wierzchołków x ilość wierzchołków.");

            if (IsWeighted == false)
            {
                IsWeighted = true;
                FillMatrixOfWeights();
            }

            for (var i = 0; i < weights.GetLength(0); i++)
                for (var j = i + 1; j < weights.GetLength(1); j++)
                {
                    if (i == j) _matrixOfWeights[i, j] = 0;
                    else
                    {
                        _matrixOfWeights[i, j] = weights[i, j];
                        _matrixOfWeights[j, i] = weights[i, j];
                    }
                }

            foreach (var edge in _edges)
            {
                var index1 = _vertices.IndexOf(edge.Begin);
                var index2 = _vertices.IndexOf(edge.End);

                edge.Weight = _matrixOfWeights[index1, index2];
            }
        }

        public void RemoveAllWeights()
        {
            if (IsWeighted)
            {
                IsWeighted = false;

                FillMatrixOfWeights();

                foreach (var edge in _edges)
                {
                    edge.Weight = 1;
                }
            }
        }

        public Vertex GetVertex(string name)
        {
            var vertex = _vertices.FirstOrDefault(x => x.Name == name);
            if (vertex == null)
                throw new ArgumentException("Błąd: wierzchołek o podanej nazwie nie istnieje w aktualnym grafie.");
            return vertex;
        }

        public List<Vertex> GetAllVerticesToList()
        {
            return _vertices;
        }

        public void AddVertex(Vertex vertex)
        {
            if (vertex == null || vertex.Name == "")
                throw new ArgumentException("Błąd: podany wierzchołek nie może być nullem.");

            var checkNameOfVertices = _vertices.FirstOrDefault(x => x.Name == vertex.Name);
            if (checkNameOfVertices != null) throw new ArgumentException("Błąd: wierzchołek o podanej nazwie już istnieje.");

            var countOfVertices = _vertices.Count;

            AddVertexToAdjacencyMatrix(countOfVertices);
            AddVertexToMatrixOfWeights(countOfVertices);
            _vertices.Add(vertex);
        }

        public void RemoveVertex(Vertex vertex)
        {
            if (vertex == null) throw new ArgumentException("Podany wierzchołek nie może być nullem!");

            var newVertex = _vertices.FirstOrDefault(x => x.Name == vertex.Name);
            if (newVertex == null) throw new ArgumentException("Podany wierzchołek nie istnieje!");

            var indexOfVertex = _vertices.IndexOf(newVertex);
            var countOfVertices = _vertices.Count;

            RemoveVertexFromAdjacencyMatrix(countOfVertices, indexOfVertex);
            RemoveVertexFromMatrixOfWeights(countOfVertices, indexOfVertex);
            _vertices.Remove(newVertex);
            RemoveNeighbor(newVertex);
            RemoveEdgesFromVertex(newVertex);
        }

        public void RemoveAllVertex()
        {
            _edges = new List<Edge>();
            _vertices = new List<Vertex>();
            _adjacencyMatrix = new int[0,0];
            _matrixOfWeights = new double[0,0];
        }

        public Edge GetEdge(Vertex vertexBegin, Vertex vertexEnd)
        {
            var edge =_edges.FirstOrDefault(x =>((x.Begin == vertexBegin && x.End == vertexEnd) || (x.End == vertexBegin && x.Begin == vertexEnd)));
            if (edge == null) throw new ArgumentException("Błąd: krawędź o podanych końcach nie istnieje w aktualnym grafie.");

            return edge;
        }

        public List<Edge> GetAllEdgesToList()
        {
            return _edges;
        }

        public void AddEdge(Edge edge)
        {
            if (edge == null) throw new ArgumentException("Błąd: podana krawędź nie może być nullem!");
            var beginVertex = _vertices.FirstOrDefault(x => x.Name == edge.Begin.Name);
            var endVertex = _vertices.FirstOrDefault(b => b.Name == edge.End.Name);

            if (beginVertex == null || endVertex == null)throw new ArgumentException("Błąd: podana krawędź nie może zostać dodana,poinieważ wierzchołek początkowy i/lub końcowy nie istnieje w aktualnym grafie.");

            var checkEdge = _edges.FirstOrDefault(x => (x.Begin == beginVertex && x.End == endVertex) || (x.End == beginVertex && x.Begin == endVertex));
            if (checkEdge != null) throw new ArgumentException("Błąd: podana krawędź nie może zostać dodana, gdyż już istnieje.");

            var newEdge = new Edge(beginVertex, endVertex)
                              {
                                  Weight = 1
                              };
            _edges.Add(newEdge);

            var iIndex = _vertices.IndexOf(beginVertex);
            var jIndex = _vertices.IndexOf(endVertex);

            beginVertex.Neighbors.Add(endVertex);
            endVertex.Neighbors.Add(beginVertex);
            _adjacencyMatrix[iIndex, jIndex] = 1;
            _adjacencyMatrix[jIndex, iIndex] = 1;
        }

        public void RemoveEdge(Edge edge)
        {
            if (edge == null) throw new ArgumentException("Błąd: podana krawędź nie może być nullem.");
            var checkEdge = _edges.FirstOrDefault(x => x == edge);
            if (checkEdge == null) throw new ArgumentException("Błąd: nie można usunąć- podana krawędź nie istnieje w aktualnym grafie.");

            _edges.Remove(edge);

            var iIndex = _vertices.IndexOf(edge.Begin);
            var jIndex = _vertices.IndexOf(edge.End);

            edge.Begin.Neighbors.Remove(edge.End);
            edge.End.Neighbors.Remove(edge.Begin);

            _adjacencyMatrix[iIndex, jIndex] = 0;
            _adjacencyMatrix[jIndex, iIndex] = 0;
        }

        public void RemoveAllEdges()
        {
            _edges = new List<Edge>();

            for (var i = 0; i < _adjacencyMatrix.GetLength(0); i++)
                for (var j = 0; j < _adjacencyMatrix.GetLength(1); j++)
                    _adjacencyMatrix[i, j] = 0;
        }

        public int[,] GetAdjacencyMatrix()
        {
            return _adjacencyMatrix;
        }

        public double[,] GetMatrixOfWeights()
        {
            return _matrixOfWeights;
        }

        #endregion

        #region Algorithms

        public List<Vertex> BfsAlgorithm(Vertex vertex)
        {
            if (vertex == null) throw new ArgumentException("Błąd: podany wierzchołek nie może być nullem.");

            var vertices = CopyOfTheListOfVertices();
            var newVertex = vertices.FirstOrDefault(x => x.Name == vertex.Name);

            if (newVertex == null) throw new ArgumentException("Błąd: podany wierzchołek nie istnieje w aktualnym grafie.");


            foreach (var v in vertices)
            {
                v.Colour = "white";
                v.Distance = int.MaxValue;
                v.Parent = null;
            }

            newVertex.Distance = 0;
            newVertex.Colour = "gray";

            var grayVertices = new List<Vertex>
                                   {
                                       newVertex
                                   };

            while (grayVertices.Count > 0)
            {
                var lastVertex = grayVertices[0];
                foreach (var v in lastVertex.Neighbors.Where(v => v.Colour == "white"))
                {
                    v.Colour = "gray";
                    v.Distance = lastVertex.Distance + 1;
                    v.Parent = lastVertex;
                    grayVertices.Add(v);
                }
                lastVertex.Colour = "black";
                grayVertices.Remove(lastVertex);
            }
            return vertices.OrderBy(x => x.Distance).ToList();
        }

        public void ShowBfsResult(List<Vertex> vertices, Graphics pictureOfGraph, int time)
        {
            if (vertices == null || vertices.Count <= 0)
                throw new ArgumentException("Lista wierzchołków jest nullem lub jest pusta!");
            DrawVertices(pictureOfGraph);

            var distance = 1;
            var indexOfColors = 0;

            foreach (var vertex in vertices)
            {
                if (vertex.Distance == distance && vertex.Parent != null)
                {
                    Thread.Sleep(time);
                    DrawEdge(pictureOfGraph, vertex, vertex.Parent, _graphStyles.EdgePenList[indexOfColors]);
                    DrawVertex(pictureOfGraph, vertex);
                    DrawVertex(pictureOfGraph, vertex.Parent);
                }
                else if (vertex.Distance != 0)
                {
                    indexOfColors++;
                    if (indexOfColors >= _graphStyles.EdgePenList.Count) indexOfColors = 0;
                    distance++;

                    if (vertex.Parent != null)
                    {
                        Thread.Sleep(time);
                        DrawEdge(pictureOfGraph, vertex.Parent, vertex, _graphStyles.EdgePenList[indexOfColors]);
                        DrawVertex(pictureOfGraph, vertex);
                        DrawVertex(pictureOfGraph, vertex.Parent);
                    }
                }
            }
        }

        public List<Vertex> DfsAlgorithm(Vertex vertex)
        {
            if (vertex == null) throw new ArgumentException("Błąd: podany wierzchołek nie może być nullem.");

            var vertices = CopyOfTheListOfVertices();
            var newVertex = vertices.FirstOrDefault(x => x.Name == vertex.Name);

            if (newVertex == null) throw new ArgumentException("Błąd: podany wierzchołek nie istnieje w aktualnym grafie.");

            var newVertices = new List<Vertex>();

            var index = vertices.IndexOf(newVertex);

            for (var i = index; i < vertices.Count; i++)
            {
                newVertices.Add(vertices[i]);
            }

            for (var i = 0; i < index; i++)
            {
                newVertices.Add(vertices[i]);
            }

            foreach (var v in newVertices)
            {
                v.Colour = "white";
                v.VisitTime = v.ProcessTime = 0;
                v.Parent = null;
            }
            _stepDfsAlgorithm = 0;

            VisitNode(vertex);

            foreach (var v in newVertices.Where(v => v.Colour == "white"))
            {
                VisitNode(v);
            }
            return newVertices.OrderBy(x => x.VisitTime).ToList();
        }

        public void ShowDfsResult(List<Vertex> vertices, Graphics pictureOfGraph, int time)
        {
            if (vertices == null || vertices.Count <= 0)
                throw new ArgumentException("Błąd: lista wierzchołków jest nullem lub jest pusta!");
            DrawVertices(pictureOfGraph);

            foreach (var vertex in vertices)
            {

                if (vertex.Parent != null)
                {
                    Thread.Sleep(time);
                    DrawEdge(pictureOfGraph, vertex.Parent, vertex, _graphStyles.EdgePen);
                    DrawVertex(pictureOfGraph, vertex);
                    DrawVertex(pictureOfGraph, vertex.Parent);
                }
            }
        }

        public List<Vertex> PrimsAlgorithm(Vertex vertex)
        {
            if (vertex == null) throw new ArgumentException("Błąd: podany wierzchołek nie może być nullem.");

            var vertices = CopyOfTheListOfVertices();
            var newVertex = vertices.FirstOrDefault(x => x.Name == vertex.Name);

            if (newVertex == null) throw new ArgumentException("Błąd: podany wierzchołek nie istnieje w aktualnym grafie.");

            var stepOfAlghoritm = 0;

            foreach (var v in vertices)
            {
                v.Key = Int16.MaxValue;
                v.Parent = null;
            }
           
            newVertex.Key = 0;
            newVertex.VisitTime = stepOfAlghoritm;

            var queueOfVertices = vertices.ToList();

            while (queueOfVertices.Count > 0)
            {
                queueOfVertices = queueOfVertices.OrderBy(x => x.Key).ToList();
                var u = queueOfVertices[0];
                foreach (var v in u.Neighbors)
                {
                    var weight = FindWeight(u, v);
                    if (queueOfVertices.FirstOrDefault(x => x == v) != null && weight < v.Key)
                    {
                        v.Parent = u;
                        v.Key = weight;
                    }
                }
                u.VisitTime = ++stepOfAlghoritm;
                queueOfVertices.Remove(u);
            }
            return vertices.OrderBy(x => x.VisitTime).ToList();
        }

        public void ShowPrimsResult(List<Vertex> vertices, Graphics pictureOfGraph, int time)
        {
            if (vertices == null || vertices.Count <= 0)
                throw new ArgumentException("Lista wierzchołków jest nullem lub jest pusta!");
            DrawVertices(pictureOfGraph);

            var indexOfColors = 0;

            foreach (var vertex in vertices.Where(vertex => vertex.Parent != null))
            {
                Thread.Sleep(time);

                if (indexOfColors >= _graphStyles.EdgePenList.Count) indexOfColors = 0;
                indexOfColors++;

                DrawEdge(pictureOfGraph, vertex, vertex.Parent, _graphStyles.EdgePen);
                DrawWeight(pictureOfGraph, vertex, vertex.Parent);
                DrawVertex(pictureOfGraph, vertex);
                DrawVertex(pictureOfGraph, vertex.Parent);
            }
        }

        public List<Vertex> DijkstrasAlgorithm(Vertex vertex)
        {
            if (vertex == null) throw new ArgumentException("Błąd: podany wierzchołek nie może być nullem.");

            var vertices = CopyOfTheListOfVertices();
            var newVertex = vertices.FirstOrDefault(x => x.Name == vertex.Name);

            if (newVertex == null) throw new ArgumentException("Błąd: podany wierzchołek nie istnieje w aktualnym grafie.");

            foreach (var v in vertices)
            {
                v.Distance = Int16.MaxValue;
                v.Parent = null;
            }

            newVertex.Distance = 0;

            var queueOfVertices = vertices.ToList();

            while (queueOfVertices.Count > 0)
            {
                queueOfVertices = queueOfVertices.OrderBy(x => x.Distance).ToList();
                var u = queueOfVertices[0];

                foreach (var v in u.Neighbors)
                {
                    var weight = FindWeight(u, v);
                    if (v.Distance > u.Distance + weight)
                    {
                        v.Distance = u.Distance + weight;
                        v.Parent = u;
                    }
                }
                queueOfVertices.Remove(u);
            }
            return vertices.OrderBy(x => x.Distance).ToList();
        }

        public void ShowDijkstrasResult(List<Vertex> vertices, Graphics pictureOfGraph, int time)
        {
            if (vertices == null || vertices.Count <= 0) throw new ArgumentException("Lista wierzchołków jest nullem lub jest pusta!"); // !!!!!!!!!!
            
            DrawVertices(pictureOfGraph);

            var indexOfColors = 0;

            foreach (var vertex in vertices.Where(vertex => vertex.Parent != null))
            {
                Thread.Sleep(time);

                if (indexOfColors >= _graphStyles.EdgePenList.Count) indexOfColors = 0;
                indexOfColors++;

                DrawEdge(pictureOfGraph, vertex, vertex.Parent, _graphStyles.EdgePen);
                DrawWeight(pictureOfGraph, vertex, vertex.Parent);
                DrawVertex(pictureOfGraph, vertex);
                DrawDijkstrasWeightOfVertex(pictureOfGraph, vertex);
                DrawVertex(pictureOfGraph, vertex.Parent);
            }
        }

        private void VisitNode(Vertex vertex)
        {
            vertex.Colour = "gray";
            _stepDfsAlgorithm++;
            vertex.VisitTime = _stepDfsAlgorithm;

            foreach (var v in vertex.Neighbors.Where(v => v.Colour == "white"))
            {
                v.Parent = vertex;
                VisitNode(v);
            }
            vertex.Colour = "black";
            _stepDfsAlgorithm++;
            vertex.ProcessTime = _stepDfsAlgorithm;
        }

        private double FindWeight(Vertex u, Vertex v)
        {
            var edge =_edges.FirstOrDefault(x =>(x.Begin.Name == u.Name && x.End.Name == v.Name) || (x.Begin.Name == v.Name && x.End.Name == u.Name));
            return edge.Weight;
        }

        #endregion
    }
}