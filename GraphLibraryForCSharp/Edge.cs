﻿namespace GraphLibraryForCSharp
{
    public class Edge
    {
        /// <summary>
        /// Tworzy nową krawędź.
        /// </summary>
        /// <param name="beginVertex">Wierzchołek początkowy krawędzi.</param>
        /// <param name="endVertex">Wierzchołek końcowy krawędzi.</param>
        public Edge(Vertex beginVertex, Vertex endVertex)
        {
            Begin = beginVertex;
            End = endVertex;
        }

        /// <summary>
        /// Wierzchołek początkowy krawędzi (w grafach nieskierowanych nie jest ważna kolejność wierzchołków: początkowego i końcowego).
        /// </summary>
        public Vertex Begin { get; internal set; }

        /// <summary>
        /// Wierzchołek końcowy krawędzi (w grafach nieskierowanych nie jest ważna kolejność wierzchołków: początkowego i końcowego).
        /// </summary>
        public Vertex End { get; internal set; }

        /// <summary>
        /// Waga krawędzi.
        /// </summary>
        public double Weight { get; set; }
    }
}